# Python Multiprocessing과 Multithreading<sup>[1](#footnote_1)</sup>

널리 사용되는 프로그래밍 언어인 Python은 멀티프로세싱과 멀티스레딩을 포함하여 프로그램 실행을 향상시키는 다양한 기술을 제공한다. 프로그램 실행의 기본을 이해해 보도록 하자.

CPU에서 Python 프로그램이 작동하려면 CPU 코어, RAM(랜덤 액세스 메모리) 및 저장 디스크와 같은 몇 가지 주요 구성 요소의 조정이 필요하다.

앞서 언급 한 구성 요소를 사용하는 Python 프로그램 작업의 단순화된 흐름도는 다음과 같다.

![](./1_fu6AKzzE6l1pBDGF_AeUnw.webp)

이 다이어그램에서,

- 프로그램은 처음에 디스크에 저장된다.
- 프로그램이 실행되면 디스크에서 RAM으로 로드된다.
- CPU는 RAM에서 프로그램 명령을 가져와 실행한다.
- CPU는 실행 중 필요에 따라 RAM에서 데이터를 읽거나 쓸 수 있다.
- 프로그램 흐름은 완료될 때까지 계속된다.

Python에서 멀티프로세싱 또는 멀티스레딩을 사용할지 여부를 결정하려면, 작업의 특성과 요구 사항을 고려하여야 한다. 다음은 멀티프로세싱과 멀티스레딩 중 하나를 선택할 때 고려해야 할 몇 가지 요소이다.

## 1. 작업 타입(Task Type):

- CPU-bound 작업: 이러한 작업에는 집중적인 계산 또는 데이터 처리가 포함되며 다중 프로세싱을 통해 여러 CPU 코어를 효과적으로 활용할 수 있다.
- I/O 바운드 작업: 이러한 작업에는 I/O 작업(예: 네트워크 요청, 파일 I/O)을 기다리는 작업이 포함된다. 멀티스레딩은 동시 실행과 응답성 향상을 허용하므로 이러한 작업에 적합한 경우가 많다.

## 2. 병렬(Parallelism):

- 멀티프로세싱: 작업을 병렬로 실행할 수 있는 독립적인 하위 작업으로 나눌 수 있는 경우 멀티프로세싱을 사용하면 여러 프로세스에 워크로드를 분산하고 여러 CPU 코어를 활용할 수 있다.
- 멀티스레딩: 동시 I/O 작업이 필요한 작업이나 더 작은 단위로 병렬화할 수 있는 작업(예: 개별 데이터 항목 처리)의 경우 멀티스레딩을 사용하면 여러 스레드가 동시에 작업할 수 있으므로 이점을 얻을 수 있다.

다음은 대규모 데이터 집합의 평균을 계산하는 CPU 집약적인 프로그램에 멀티프로세싱과 멀티스레딩을 사용하는 세 가지 예이다.

Example 1: 멀티프로세싱 또는 멀티스레딩을 사용하지 않는 경우

```python
import numpy as np
import time

# Generate random 100 million data points
n = 100000000
d = np.random.rand(n)

def calculate_mean():
    sum_ = np.sum(d)
    mean = sum_ / n
    return mean

# Time the execution
start_time = time.time()
m = calculate_mean()
end_time = time.time()
execution_time = end_time - start_time

# Print the results
print("Mean calculation results:")
print("Mean:", m)
print("Execution time:", execution_time, "seconds")
```

```
Mean calculation results:
Mean: 0.49994777164597376
Execution time: 20.90457510948181 seconds
```

이 예에서는 병렬 처리를 활용하지 않고 순차적으로 계산을 수행하였다.

Example 2: 멀티프로세싱 사용

```python
from multiprocessing import Process, Queue
import math
import time

def mean_multiprocessing(s, e, q, data):
    total_sum = 0
    for i in range(s, e + 1):
        total_sum += data[i]
    mean = total_sum / (e - s + 1)
    q.put(mean)

if __name__ == '__main__':
    # Generate random 100 million data points
    n = 100000000
    data = [i / n for i in range(n)]

    n1 = math.floor(n / 2)

    q = Queue()
    p1 = Process(target=mean_multiprocessing, args=(0, n1, q, data))
    p2 = Process(target=mean_multiprocessing, args=(n1 + 1, n - 1, q, data))

    start_time = time.time()

    p1.start()
    p2.start()

    p1.join()  # Wait till p1 finishes
    p2.join()

    mean = 0
    while not q.empty():
        mean += q.get()
    mean /= 2

    end_time = time.time()

    execution_time = end_time - start_time

    # Print the results
    print("Mean calculation results:")
    print("Mean:", mean)
    print("Execution time using multiprocessing:", execution_time, "seconds")
```

```
Mean calculation results:
Mean: 11.001178979873657
Execution time using multiprocessing: 0.4999477716457993 seconds
```

이 예에서는 데이터 집합을 두 부분으로 분할하고 여러 CPU 코어를 활용하여 두 개의 개별 프로세스를 사용하여 동시에 계산을 수행한다.

Example 3: 멀티스레딩 사용

```python
from threading import Thread
import math
import time

means = [0, 0]

def calculate_mean(start, end, thread_num):
    sum_ = 0
    for i in range(start, end+1):
        sum_ += d[i]
    
    mean = sum_ / (end - start + 1)
    means[thread_num] = mean

n = 100000000
n1 = math.floor(n/2)
d = [1] * n

t1 = Thread(target=calculate_mean, args=(0, n1, 0))
t2 = Thread(target=calculate_mean, args=(n1+1, n-1, 1))

start_time = time.time()

t1.start()
t2.start()

t1.join()
t2.join()

mean1 = means[0]
mean2 = means[1]
mean = (mean1 + mean2) / 2

end_time = time.time()

execution_time = end_time - start_time

print("Mean calculation results:")
print("Mean:", mean)
print("Execution time using multithreading:", execution_time, "seconds")
```

```
Mean calculation results:
Mean: 19.538660049438477
Execution time using multiprocessing: 0.4999477716457993 seconds
```

이 예에서는 두 개의 스레드를 사용하여 데이터 집합을 두 부분으로 나누어 동시에 계산을 수행한다. 그러나 Python의 GIL(전역 인터프리터 잠금(Global Interpreter Lock)) 제한으로 인해 진정한 병렬 처리가 이루어지지 않고 성능 개선이 제한되었다.

GIL은 한 번에 하나의 스레드만 Python 코드를 실행할 수 있도록 허용하는 잠금 장치이다. 프로그램에 여러 개의 스레드가 있더라도 다른 스레드가 차례를 기다리는 동안 하나의 스레드만 Python 코드를 실행할 수 있다.

스레드가 네트워크나 디스크에서 데이터를 기다리는 동안 동시 입출력 작업이 일어나지 않는다면 멀티스레딩의 이점은 더욱 감소한다.

스레드가 I/O를 기다리는 동안 다른 작업을 수행할 기회가 없어지고 GIL 제한이 더욱 두드러지기 때문이다.

따라서 동시 I/O 작업이 없는 CPU 집약적인 작업의 경우, 최적의 성능을 달성하기 위해 Python의 멀티스레딩이 최선의 선택이 아닐 수 있다. 대신 멀티프로세싱을 사용하는 것이 좋다.


<a name="footnote_1">1</a>: 이 페이지는 [Python Multiprocessing과 Multithreading](./multiprocessingnMultithreading.md)는 [Understanding Python Multiprocessing and Multithreading:](https://medium.com/@pritam7798sonawane/understanding-python-multiprocessing-and-multithreading-eda6e68d81b6)을 편역한 것임.
